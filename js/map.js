
var sel_filters = {
	'wdpaid': null,
	'region': null,
	'iso3':null
};

var dt_table;


//TODO move into Dependancy with PA module.
var DOPAgetWdpaExtent = "https://rest-services.jrc.ec.europa.eu/services/d6biopamarest/d6biopama/get_wdpa_extent?format=json&wdpa_id=";
var mapPointHostURL = "https://tiles.biopama.org/BIOPAMA_point_2";
var mapPolyHostURL = "https://tiles.biopama.org/BIOPAMA_poly_2";
var mapPaLayer = "WDPA2019MayPoly";
var mapCountryLayer = "ACP_Countries";
var mapPaLabelsLayer = "WDPA2019MayPolyPoints";
var mapPaPointLayer = "WDPA2019MayPoints";
var mapSubRegionLayer = "ACP_SubGroups";
var mapSubRegionPointLayer = "ACP_SubGroups_points";
var regionSummary = {};
var countrySummary = {};//#of applications, total budget

jQuery(document).ready(function($) {
	function createDTables(){
		dt_table = $('#table_priority_actions').DataTable({
			"columns" : [
				{ "data" : "region.name", "width": "5%" },
				{ "data" : "acp_country", "width": "5%" },
				{ "data" : "priority_area_targeted", "width": "10%" },
				{ "data" : "wdpa_id", "width": "10%" },
				{ "data" : "lead_applicant", "width": "5%" },
				{ "data" : "type_of_diagnostic_tool.ref_doc", "width": "30%" },
				{ "data" : "priority_activities_targeted", "width": "30%" },
				{ "data" : "total_funding_requested", "width": "5%" },
				{ "data" : "iso3", "visible": false }
			],
			dom: 'Bfrtip',
			buttons: [
				{
					extend: 'excel',
					className: 'btn btn-info',
				},
				{
					extend: 'pdfHtml5',
					orientation: 'landscape',
					pageSize: 'LEGAL',
					className: 'btn btn-info',
					customize: function (doc){
					  doc['header'] = 'Data Source: BIOPAMA Action Component Portal | https://action.biopama.org/';
					  doc.content[1].table.widths = ['7%','7%','10%','10%','5%','27%','27%','7%']
					}
				}
			]
		});
		dt_table.on( 'search.dt', function () {
			updateMapExtent(false);
		} );
		$('button.dt-button').removeClass("dt-button");
		dt_table.buttons().container().appendTo( $('.dt-buttons', dt_table.table().container() ) );
		$('#spinner').hide();
	}
	$( '#ac-country', this ).on( 'keyup change', function () {
		if (this.value == "All"){
			dt_table.column(1).search('').draw();
		} else if ( dt_table.column(1).search() !== $( "#ac-country option:selected" ).text() ) {
			dt_table.column(1).search( $( "#ac-country option:selected" ).text() ).draw();
			updateMapExtent(false);
		}
	} );
	$( '#ac-region', this ).on( 'keyup change', function () {
		if (this.value == "All"){
			dt_table.column(0).search('').draw();
		} else if ( dt_table.column(0).search() !== $( "#ac-region option:selected" ).text() ) {
			dt_table.column(0).search( $( "#ac-region option:selected" ).text() ).draw();
			updateMapExtent(false);
			pop_country_filters();
		}
	} );
	
	$( '#reset_filters', this ).on( 'click', function () {
		$('#ac-region option[value=All]').prop('selected', true);
		dt_table.column(0).search('');
		$('#ac-country option[value=All]').prop('selected', true);
		dt_table.column(1).search('').draw();
		updateMapExtent();
	} );
	$( '#regional-sum', this ).on( 'click', function () {
		if ($('#map .pop-summary')[0]){
			$('#app-sum-legend').hide();
			$('.mapboxgl-popup-close-button').trigger("click");
		} else {
			var features = mymap.queryRenderedFeatures({layers: ['regionLabels']});
			$('#app-sum-legend').show();
			$.each(features,function(idx,obj){
				var funding;
				var numProjects;
				switch (obj.properties.Group) {
					case "Western Africa": 
						funding = regionSummary["West Africa"].totalFunding;
						numProjects = regionSummary["West Africa"].totalProjects;
						break;
					default: 
						funding = regionSummary[obj.properties.Group].totalFunding;
						numProjects = regionSummary[obj.properties.Group].totalProjects;
				}
				funding = (funding).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'); 
				var popup = new mapboxgl.Popup({ closeOnClick: false })
				.setLngLat(obj.geometry.coordinates)
				.setHTML('<div class="pop-summary"><H2>' + numProjects + '</H2><H6>' + funding + '</H6></div>')
				.addTo(mymap);
			});
		}
	} );
	$( '#country-sum', this ).on( 'click', function () {
		if ($('#map #country-help-text:visible')[0]){
			$('#country-help-text').hide();
			$('#app-sum-legend').hide();
			mymap.setLayoutProperty("countryMask", 'visibility', 'none');
			$('.mapboxgl-popup-close-button').trigger("click");
		} else {
			$('#country-help-text').show();
			$('#app-sum-legend').show();
			mymap.setLayoutProperty("countryMask", 'visibility', 'visible');
		}
		
	});
	function updateMapExtent(global = true){
		var applicationsByCountry = [];
		var applicationsByCountry1 = ['in', 'iso3'];
		var applicationsByCountry2 = ['in', 'iso3'];
		var applicationsByCountry3 = ['in', 'iso3'];
		var applicationsByCountry4 = ['in', 'iso3'];
		var applicationsByCountry5 = ['in', 'iso3'];
		var applicationsByWDPA = ['in', 'WDPAID'];
		var counts = {};
		var countryCodes = dt_table.column(8, { filter : 'applied'}).data();
		var WDPAIDs = dt_table.column(3, { filter : 'applied'}).data();
		$.each(countryCodes,function(idx,obj){
			if (obj.indexOf(',')){
				var res = obj.split(", ");
				applicationsByCountry = applicationsByCountry.concat(res);
			} else {
				applicationsByCountry.push(obj)
			}
		});
		for (var i = 0; i < applicationsByCountry.length; i++) {
		  var num = applicationsByCountry[i];
		  counts[num] = counts[num] ? counts[num] + 1 : 1;
		}
		for (key in counts) {
			switch (counts[key]) {
				case 1: 
					applicationsByCountry1.push(key);
					break;
				case 2: 
					applicationsByCountry2.push(key);
					break;
				case 3: 
					applicationsByCountry3.push(key);
					break;
				case 4: 
					applicationsByCountry4.push(key);
					break;
				default: 
					applicationsByCountry5.push(key);
			}
		}
		$.each(WDPAIDs,function(idx,obj){
			var wdpas = stripHtml(obj);
			if (wdpas.length > 1){
				wdpas = wdpas.trim();
				if (wdpas.indexOf(',')){
					var res = wdpas.split(", ");
					$.each(res,function(idx,obj){ 
						applicationsByWDPA.push(parseInt(obj))
					});
				} else  {
					applicationsByWDPA.push(parseInt(wdpas))
				}
			}
		}); 
		if(applicationsByCountry.length > 25) global = true;
		mymap.setFilter("wdpaApplications", applicationsByWDPA);
		mymap.setLayoutProperty("wdpaApplications", 'visibility', 'visible');	
		mymap.setFilter("countryApplications1", applicationsByCountry1);	
		mymap.setLayoutProperty("countryApplications1", 'visibility', 'visible');
		mymap.setFilter("countryApplications2", applicationsByCountry2);	
		mymap.setLayoutProperty("countryApplications2", 'visibility', 'visible');
		mymap.setFilter("countryApplications3", applicationsByCountry3);	
		mymap.setLayoutProperty("countryApplications3", 'visibility', 'visible');
		mymap.setFilter("countryApplications4", applicationsByCountry4);	
		mymap.setLayoutProperty("countryApplications4", 'visibility', 'visible');
		mymap.setFilter("countryApplications5", applicationsByCountry5);	
		mymap.setLayoutProperty("countryApplications5", 'visibility', 'visible');
		if (global){
			mymap.flyTo({center: [26, -6.66], zoom: 2});
		} else {
			url = 'https://rest-services.jrc.ec.europa.eu/services/d6biopamarest/d6biopama/get_bbox_for_countries_dateline_safe?iso3codes='+countryCodes.join(',').replace(/[\s]/g,"")+'&format=json&includemetadata=false';
			$.getJSON(url,function(response){
				mymap.fitBounds(jQuery.parseJSON(response.records[0].get_bbox_for_countries_dateline_safe));
			});
		}
	}
	
	function stripHtml(html){
	   var tmp = document.createElement("DIV");
	   tmp.innerHTML = html;
	   return tmp.textContent || tmp.innerText || "";
	}

	function pop_region_filters(regions){
		var select_region = document.getElementById("ac-region");
		$.each(regions,function(index, value){
			var option = document.createElement("option");
			option.value = value
			option.text = value
			select_region.appendChild(option);
		});
	}
	function pop_country_filters(){
		var countryCodes = [];
		var currentCountryNames = dt_table.column(1, { filter : 'applied'}).data();
		var currentCountryCodes = dt_table.column(8, { filter : 'applied'}).data();
		//empty filters
		$.each($('#ac-country'),function(){
		  $(this).find('option').not(':first').remove();
		});
		//join all arrays of countries to have full list of codes
		$.each(currentCountryCodes,function(idx,obj){
			if (obj.indexOf(',')){
				var codeSplit = obj.split(", ");
				var nameSplit = currentCountryNames[idx].split(", ");
				$.each(codeSplit,function(idx,obj){
					if(countryCodes.indexOf(obj) === -1) {
						countryCodes.push(obj);
						createCountryOption(obj, nameSplit[idx]);
					}
				});
			} else {
				if(countryCodes.indexOf(obj) === -1) {
					countryCodes.push(obj);
					createCountryOption(obj, currentCountryNames[idx]);
				}
			}
		});
		var options = $('select#ac-country option:not(:first)');
		var arr = options.map(function(_, o) { return { t: $(o).text(), v: o.value }; }).get();
		arr.sort(function(o1, o2) { return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0; });
		options.each(function(i, o) {
		  o.value = arr[i].v;
		  $(o).text(arr[i].t);
		});
	}
	function createCountryOption(countryCode, countryName){
		var select_country = document.getElementById("ac-country");
		var option = document.createElement("option");
		option.value = countryCode.replace(/[\n\t\r]/g,"");
		option.text = countryName.replace(/[\n\t\r]/g,"");
		select_country.appendChild(option);
	}
	
	function generateRestArgs(){
		var cleanArgs = '';
		for (var propName in sel_filters) { 
			if ((sel_filters[propName] != null) || (sel_filters[propName] != undefined)) {
			  cleanArgs += '&' + propName + '=' + sel_filters[propName];
			}
		}		
		return cleanArgs;
	}
	
	function setTableData(){
		$('#spinner').show();
		
		var restArguments = generateRestArgs();
		
		var url = "https://api-app.biopama.org/api/application/medium-grants/78ad6462-4fbf-4906-8fd2-39b7f2b4671b";
		
		$.getJSON(url,function(response){
			var countryNames = [];
			var countryIDs = [];
			var regions = [];
			$('#spinner').hide();
			$.each(response,function(idx,obj){
				var currentCountries = "";
				var currentISO = "";
				var funding = obj.total_funding_requested;
				var region = obj.region.name;
				region = region.replace(/[\n\t\r]/g,"");
				obj.region.name = region;
				funding = parseInt(funding.replace(/[^\d.-]/g,''));
				if (funding > 400000) funding = 400000;
				if(regions.indexOf(region) === -1){
					regions.push(region); 
					regionSummary[region] = {
						totalProjects: 0,
						totalFunding: 0
					}
				}
				regionSummary[region].totalProjects += 1;
				regionSummary[region].totalFunding += funding;
				$.each(obj.acp_country,function(idx,obj){
					var countryISO3 = switchCountryID(obj.country_id);
					if(countryIDs.indexOf(countryISO3) === -1) {
						countryIDs.push(countryISO3); //collect all countries to zoom to the group
						countryNames.push(obj.name); //collect all country names for the filter
						countrySummary[countryISO3] = {
							totalProjects: 0,
							totalFunding: 0
						}
					}
					countrySummary[countryISO3].totalProjects += 1;
					countrySummary[countryISO3].totalFunding += funding;
					currentCountries += obj.name + ", " ;
					currentISO += countryISO3 + ", " ;
				});
				var WDPAIDs = obj.wdpa_id;
				WDPAIDs = WDPAIDs.replace(/[^0-9\s]/g,'');
				WDPAIDs = WDPAIDs.trim();
				WDPAIDs = WDPAIDs.replace(/\s+/g, ", ");
				var WDPAarray = WDPAIDs.split(', ');
				$.each(WDPAarray,function(idx, obj){
					WDPAarray[idx] = ' <a href="#" onclick="return false;" class="app-wdpa" title="Zoom to this PA in the map">'+obj+'</a>';
				});
				WDPAarray = WDPAarray.toString();
				funding = (funding).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'); 
				obj.total_funding_requested = funding;
				obj.wdpa_id = WDPAarray;
				currentCountries = currentCountries.substring(0, currentCountries.length - 2); //remove the trailing comma
				currentISO = currentISO.substring(0, currentISO.length - 2); //remove the trailing comma
				obj.acp_country = currentCountries;
				obj.iso3 = currentISO;
			});
			console.log(countrySummary);
			dt_table.clear();
			dt_table.rows.add(response).draw();
			
			$('#table_priority_actions tbody').on('click', 'td  .app-wdpa',function(){
				var wdpa = $(this).text();
				zoomToPA(wdpa);

				mymap.setFilter("wdpaSelected", ['==', 'WDPAID', parseInt(wdpa)]);	
				mymap.setLayoutProperty("wdpaSelected", 'visibility', 'visible');	

				$('html, body').animate({
					scrollTop: $("#map").offset().top - 100
				}, 1000);
			});

			//updateMapExtent();	

			$('#table_priority_actions tbody').on('click', 'tr',function(){
				$('#table_priority_actions tbody tr').removeClass('selected');
				$(this).addClass('selected');
				//var wdpa = $(this).find('td:nth-child(4)').text();
				//console.log(wdpa)
				//zoomToPA(wdpa)

				/* mymap.setFilter("wdpaSelected", ['==', 'WDPAID', wdpa]);	
				mymap.setLayoutProperty("wdpaSelected", 'visibility', 'visible'); */	

			});

			pop_region_filters(regions);
			pop_country_filters();
		});
		
	}
	function switchCountryID(countryCode){
		var correctISO3 = '';
		switch (countryCode) {
		  case 55: //Dominican Republic
			correctISO3 = "DOM";
			break;
		  case 53: //Belize
			correctISO3 = "BLZ";
			break;
		  case 20: //Central African Republic
			correctISO3 = "CAF";
			break;
		  case 67: //Kiribati
			correctISO3 = "KIR";
			break;
		  case 48: //Zambia
			correctISO3 = "ZMB";
			break;
		  case 14: //Senegal
			correctISO3 = "SEN";
			break;
		  case 29: //Kenya
			correctISO3 = "KEN";
			break;
		  case 19: //Chad
			correctISO3 = "TCD";
			break;
		  case 46: //Seychelles
			correctISO3 = "SYC";
			break;
		  case 45: //Namibia
			correctISO3 = "NAM";
			break;
		  case 41: //Madagascar
			correctISO3 = "MDG";
			break;
		  case 4: //Cote d�Ivoire
			correctISO3 = "CIV";
			break;
		  case 59: //Jamaica
			correctISO3 = "JAM";
			break;
		  case 75: //Solomon Islands
			correctISO3 = "SLB";
			break;
		  case 49: //Zimbabwe
			correctISO3 = "ZWE";
			break;
		  case 37: //Botswana
			correctISO3 = "BWA";
			break;
		  case 18: //Cameroon
			correctISO3 = "CMR";
			break;
		  case 15: //Sierra Leone
			correctISO3 = "SLE";
			break;
		  case 22: //Democratic Republic of Congo
			correctISO3 = "COD";
			break;
		  case 63: //Suriname
			correctISO3 = "SUR";
			break;
		  default:
		}
		return correctISO3;
	}
	function zoomToPA(wdpaid){
		$.ajax({
		  url: DOPAgetWdpaExtent+wdpaid,
		  dataType: 'json',
		  success: function(d) {
			mymap.fitBounds($.parseJSON(d.records[0].extent), {
				padding: {top: 100, bottom:100, left: 100, right: 100}
			});
		  },
		  error: function() {
			console.log("Something is wrong with the REST servce for PA bounds")
		  }
		});
	}

	mapboxgl.accessToken = 'pk.eyJ1IjoiYmxpc2h0ZW4iLCJhIjoiMEZrNzFqRSJ9.0QBRA2HxTb8YHErUFRMPZg';
	var mymap = new mapboxgl.Map({
		container: 'map',
		style: 'mapbox://styles/jamesdavy/cjw25laqe0y311dqulwkvnfoc', //Andrews default new RIS v2 style based on North Star
		attributionControl: true,
		renderWorldCopies: true,
		center: [26, -6.66],
        zoom: 2,
		minZoom: 1.4,
		maxZoom: 12
	});
	var mapInteractionControls = ["touchZoomRotate", "doubleClickZoom", "keyboard", "dragPan", "dragRotate", "boxZoom", "scrollZoom"];
	mapInteractionControls.forEach(element => mymap[element].disable());

	$('#pame_assessments_map').append('<div id="help-text">Double-click to pan and zoom the map.</div>');
	$('#help-text').fadeIn();

	var mapInteractionControls = ["touchZoomRotate", "doubleClickZoom", "keyboard", "dragPan", "dragRotate", "boxZoom", "scrollZoom"];
	mymap.on("dblclick",function(){
		$('#help-text').fadeOut();
		mapInteractionControls.forEach(element => mymap[element].enable());
	})

	mymap.on("mouseout",function(){
		$('#help-text').fadeIn();
		mapInteractionControls.forEach(element => mymap[element].disable());
	});


	mymap.on('load', function () {
		var legText = ["WDPA May 2019", "Application Protected Area", "Country with 1 application", "Country with 2 applications", "Country with 3 applications", "Country with 4 applications", "Country with 5 or more applications"]
		var legColor = ["hsla(87, 47%, 53%, 0.1)", "hsla(3, 40%, 50%, 1)", "rgba(253,208,162, 0.7)", "rgba(253,174,107, 0.7)", "rgba(253,141,60, 0.7)", "rgba(230,85,13, 0.7)", "rgba(166,54,3, 0.7)"]
		buildMapLegend(legText, legColor);
		mymap.addSource("BIOPAMA_Poly", {
			"type": 'vector',
			"tiles": [mapPolyHostURL+"/{z}/{x}/{y}.pbf"],
			"minZoom": 0,
			"maxZoom": 23,
		});
		mymap.addSource("BIOPAMA_Point", {
			"type": 'vector',
			"tiles": [mapPointHostURL+"/{z}/{x}/{y}.pbf"],
			"minZoom": 0,
			"maxZoom": 23,
		});
		mymap.addLayer({
			'id': 'wms-test-layer',
			'type': 'raster',
			'source': {
			'type': 'raster',
			'tiles': [
			'https://geonode-rris.biopama.org/geoserver/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=geonode%3Aklc_201508&STYLES=&SRS=EPSG%3A3857&CRS=EPSG%3A3857&TILED=true&access_token=BM4f2oL9seMTcV6kgu927gm8r81CZv&WIDTH=256&HEIGHT=256&BBOX={bbox-epsg-3857}'
			//'https://geospatial.jrc.ec.europa.eu/geoserver/biopama/wms?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=biopama%3Aklc_201508&exceptions=application%2Fvnd.ogc.se_inimage&SRS=EPSG%3A3857&STYLES=&WIDTH=256&HEIGHT=256&BBOX={bbox-epsg-3857}'
			],
			'tileSize': 256
			},
			'paint': {}
		},'country-label-sm');
		
		mymap.addLayer({
			"id": "wdpaBase",
			"type": "fill",
			"source": "BIOPAMA_Poly",
			"source-layer": mapPaLayer,
			"minzoom": 1,
            "paint": {
                "fill-color": "hsla(87, 47%, 53%, 0.1)",
            }
		});
		
		mymap.addLayer({
			"id": "countryApplications1",
			"type": "fill",
			"source": "BIOPAMA_Poly",
			"source-layer": mapCountryLayer,
			"layout": {"visibility": "none"},
			"minzoom": 1,
            "paint": {
                "fill-color": "rgba(253,208,162, 0.7)"
            }
		},"country-label-sm");
		mymap.addLayer({
			"id": "countryApplications2",
			"type": "fill",
			"source": "BIOPAMA_Poly",
			"source-layer": mapCountryLayer,
			"layout": {"visibility": "none"},
			"minzoom": 1,
            "paint": {
                "fill-color": "rgba(253,174,107, 0.7)"
            }
		},"country-label-sm");
		mymap.addLayer({
			"id": "countryApplications3",
			"type": "fill",
			"source": "BIOPAMA_Poly",
			"source-layer": mapCountryLayer,
			"layout": {"visibility": "none"},
			"minzoom": 1,
            "paint": {
                "fill-color": "rgba(253,141,60, 0.7)"
            }
		},"country-label-sm");
		mymap.addLayer({
			"id": "countryApplications4",
			"type": "fill",
			"source": "BIOPAMA_Poly",
			"source-layer": mapCountryLayer,
			"layout": {"visibility": "none"},
			"minzoom": 1,
            "paint": {
                "fill-color": "rgba(230,85,13, 0.7)"
            }
		},"country-label-sm");
		mymap.addLayer({
			"id": "countryApplications5",
			"type": "fill",
			"source": "BIOPAMA_Poly",
			"source-layer": mapCountryLayer,
			"layout": {"visibility": "none"},
			"minzoom": 1,
            "paint": {
                "fill-color": "rgba(166,54,3, 0.7)"
            }
		},"country-label-sm");
		mymap.addLayer({
			"id": "wdpaApplications",
			"type": "fill",
			"source": "BIOPAMA_Poly",
			"source-layer": mapPaLayer,
			"layout": {"visibility": "none"},
			"minzoom": 1,
            "paint": {
                "fill-color": "hsla(3, 40%, 50%, 1)"
            }
		},"country-label-sm");
		
		mymap.addLayer({
			"id": "wdpaSelected",
			"type": "line",
			"source": "BIOPAMA_Poly",
			"source-layer": mapPaLayer,
			"layout": {"visibility": "none"},
			"paint": {
				"line-color": "#679b95",
				"line-width": 2,
			},
			"transition": {
			  "duration": 300,
			  "delay": 0
			}
		});
		mymap.addLayer({
			"id": "wdpaAcpPolyLabels",
			"type": "symbol",
			"source": "BIOPAMA_Point",
			"source-layer": mapPaLabelsLayer,
			"minzoom": 5,
            "layout": {
                "text-field": ["to-string", ["get", "NAME"]],
                "text-size": 12,
                "text-font": [
                    "Arial Unicode MS Regular",
                    "Arial Unicode MS Regular"
                ]
            },
            "paint": {
                "text-halo-width": 2,
                "text-halo-blur": 2,
                "text-halo-color": "hsl(0, 0%, 100%)",
                "text-opacity": 1
            }
		}, 'country-label-sm');
		mymap.addLayer({
			"id": "wdpaAcpPointLabels",
			"type": "symbol",
			"source": "BIOPAMA_Point",
			"source-layer": mapPaPointLayer,
			"minzoom": 5,
            "layout": {
                "text-field": "{NAME}",
                "text-size": 12,
                "text-padding": 3,
				"text-offset": [0,-1]
            },
            "paint": {
                "text-color": "hsla(213, 49%, 13%, 0.95)",
                "text-halo-color": "hsla(0, 0%, 100%, .9)",
                "text-halo-width": 2,
                "text-halo-blur": 2
            }
		}, 'country-label-sm');
		mymap.addLayer({
			"id": "regionLabels",
			"type": "symbol",
			"source": "BIOPAMA_Point",
			"source-layer": mapSubRegionPointLayer,
			"maxzoom": 4,
            "layout": {
                "text-field": ["to-string", ["get", "Group"]],
                "text-font": [
                    "Arial Unicode MS Regular"
                ],
                "text-size": 22,
                "text-ignore-placement": true
            },
            "paint": {
                "text-color": "#a25b28",
                "text-halo-width": 3,
                "text-halo-color": "#fff",
                "text-halo-blur": 4
            }
		}, 'country-label-sm');
		
		mymap.addLayer({
			"id": "countryMask",
			"type": "fill",
			"source": "BIOPAMA_Poly",
			"source-layer": mapCountryLayer,
			"layout": {"visibility": "none"},
			"minzoom": 1,
            "paint": {
                "fill-color": "rgba(0,0,0, 0.01)"
            }
		},"country-label-sm");
		
		mymap.on('click', 'countryMask', function(e) {
			var iso3 = e.features[0].properties.iso3;
			console.log(e.features[0].properties)
			var numProjects = countrySummary[iso3].totalProjects;
			var funding = countrySummary[iso3].totalFunding;
			funding = (funding).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'); 
			new mapboxgl.Popup()
			.setLngLat(e.lngLat)
			.setHTML('<div class="pop-summary"><h6>'+e.features[0].properties.name_iso31+'</h6><H2>' + numProjects + '</H2><H6>' + funding + '</H6></div>')
			.addTo(mymap);
		});
		
		// Change the cursor to a pointer when the mouse is over the places layer.
		mymap.on('mouseenter', 'countryMask', function() {
			mymap.getCanvas().style.cursor = 'pointer';
		});
		 
		// Change it back to a pointer when it leaves.
		mymap.on('mouseleave', 'countryMask', function() {
			mymap.getCanvas().style.cursor = '';
		});

		var url_string = new URL( window.location.href );
		for (k in sel_filters){
			if (url_string.searchParams.get(k)){
				sel_filters[k] = url_string.searchParams.get(k);
			}
		}
		createDTables();
		setTableData();
	});
	function buildMapLegend(legText, legColor){
		var legendUnit = "<div class='legend-unit' style='display: flex; padding: 5px;'>"+
				"<div class='legend-color' style='background-color: transparent; border: 2px solid #fe971a; width:20px; height:20px;'></div>"+
				"<div class='legend-text'>&nbsp;KLC (2015)</div>"+
			"</div>";
		jQuery("#map-legend").append( legendUnit );
		jQuery(legText).each(function(index) {
			var legendUnit = "<div class='legend-unit' style='display: flex; padding: 5px;'>"+
				"<div class='legend-color' style='background-color: "+legColor[index]+"; width:20px; height:20px;'></div>"+
				"<div class='legend-text'>&nbsp;"+legText[index]+"</div>"+
			"</div>";
			jQuery("#map-legend").append( legendUnit );
		});	
	}
	
});
