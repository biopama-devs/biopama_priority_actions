<?php
namespace Drupal\biopama_priority_actions\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the biopama_priority_actions module.
 */
class PriorityActionsController extends ControllerBase {
  public function content() {
    $element = array(
	  '#theme' => 'priority_actions',
	  '#test_var' => $this->t('Test Value'),
    );
    return $element;
  }
}